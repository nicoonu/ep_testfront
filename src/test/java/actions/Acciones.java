package actions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Acciones {
	WebDriver driver;
	
	public Acciones(WebDriver driver) {
		this.driver = driver;
	}
	
	public void escribir(WebElement elemento, String texto) {
		elemento.sendKeys(texto);
	}
	
	public void click(WebElement elemento) {
		elemento.click();
	}
	
	public void seleccionarItem(Select dropDown, String item) {
		dropDown.selectByValue(item);
	}
}